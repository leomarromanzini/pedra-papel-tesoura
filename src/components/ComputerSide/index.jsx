import "./styles.css";
import PcPapel from "../../assets/PCpapel.png";
import PcPedra from "../../assets/PCpedra.png";
import PcTesoura from "../../assets/PCtesoura.png";

const ComputerSide = ({ scorePc, pcMove }) => {
  return (
    <>
      <div className="playersContainer">
        <div className="computerCard">
          <div>
            <h2 className="playersName">Super IA</h2>
            <h2>Score: {scorePc}</h2>
          </div>
          <img
            className="players"
            src="https://blog.portalpos.com.br/app/uploads/2018/04/IA-IoT-e-big-data-voc%C3%AA-sabe-como-essas-tend%C3%AAncias-impactam-na-sua-carreira.jpg"
            alt="IA"
          />
        </div>
      </div>
      {!pcMove && (
        <div>
          <img className="icons" alt="papel" src={PcPapel} />
          <img className="icons" alt="padra" src={PcPedra} />

          <img className="icons" alt="tesoura" src={PcTesoura} />
        </div>
      )}

      <div>
        {pcMove === 1 && <img className="icons" alt="papel" src={PcPapel} />}
        {pcMove === 2 && <img className="icons" alt="padra" src={PcPedra} />}
        {pcMove === 3 && (
          <img className="icons" alt="tesoura" src={PcTesoura} />
        )}
      </div>
    </>
  );
};

export default ComputerSide;
