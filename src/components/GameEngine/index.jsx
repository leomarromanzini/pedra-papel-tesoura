import React, { useState, useEffect } from "react";
import ButtonRematch from "../ButtonRematch";
import ComputerSide from "../ComputerSide";
import PlayerSide from "../PlayerSide";
import "./styles.css";

const GameEngine = () => {
  const [scorePc, setscorePC] = useState(0);
  const [scorePlayer, setScorePlayer] = useState(0);
  const [playerMove, setPlayerMove] = useState(0);
  const [pcMove, setPcMove] = useState(0);
  const [playerVictory, setPlayerVictory] = useState("");
  const [pcVictory, setPcVictory] = useState("");
  const [tie, setTie] = useState("");

  const handlePaper = () => {
    setPlayerMove(1);
    pcPLay();
  };

  const handleStone = () => {
    setPlayerMove(2);
    pcPLay();
  };

  const handleScissors = () => {
    setPlayerMove(3);
    pcPLay();
  };

  const pcPLay = () => {
    setPcMove(Math.ceil(Math.random() * 3));
  };

  useEffect(() => {
    if (pcMove) {
      if (pcMove === playerMove) {
        setTie(1);
      } else if (pcMove === 1 && playerMove === 2) {
        setPcVictory(1);
        setscorePC(scorePc + 1);
      } else if (pcMove === 1 && playerMove === 3) {
        setPlayerVictory(1);
        setScorePlayer(scorePlayer + 1);
      } else if (pcMove === 2 && playerMove === 1) {
        setPlayerVictory(1);
        setScorePlayer(scorePlayer + 1);
      } else if (pcMove === 2 && playerMove === 3) {
        setPcVictory(1);
        setscorePC(scorePc + 1);
      } else if (pcMove === 3 && playerMove === 1) {
        setPcVictory(1);
        setscorePC(scorePc + 1);
      } else if (pcMove === 3 && playerMove === 2) {
        setPlayerVictory(1);
        setScorePlayer(scorePlayer + 1);
      }
    }
  }, [pcMove]);

  const newMatch = () => {
    setPlayerMove(0);
    setPcMove(0);
    setPcVictory("");
    setPlayerVictory("");
    setTie("");
  };

  return (
    <>
      <ComputerSide scorePc={scorePc} pcMove={pcMove} />
      {tie && (
        <div className="resultMessage">
          <h2>Empate!!!</h2>
          <ButtonRematch newMatch={() => newMatch()} />
        </div>
      )}
      {playerVictory && (
        <div className="resultMessage">
          <h2>VITÓRIA!!!</h2>
          <ButtonRematch newMatch={() => newMatch()} />
        </div>
      )}
      {pcVictory && (
        <div className="resultMessage">
          {" "}
          <h2>Derrota!!!</h2>
          <ButtonRematch newMatch={() => newMatch()} />
        </div>
      )}

      <PlayerSide
        scorePlayer={scorePlayer}
        handlePaper={handlePaper}
        handleStone={handleStone}
        handleScissors={handleScissors}
        playerMove={playerMove}
      />
    </>
  );
};

export default GameEngine;
