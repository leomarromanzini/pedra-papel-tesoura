import "./styles.css";
import Papel from "../../assets/papel.png";
import Pedra from "../../assets/pedra.png";
import Tesoura from "../../assets/tesoura.png";

const PlayerSide = ({
  handlePaper,
  handleStone,
  handleScissors,
  playerMove,
  scorePlayer,
}) => {
  return (
    <>
      {!playerMove && (
        <div>
          <img
            onClick={handlePaper}
            className="icons"
            alt="papel"
            src={Papel}
          />
          <img
            onClick={handleStone}
            className="icons"
            alt="padra"
            src={Pedra}
          />
          <img
            onClick={handleScissors}
            className="icons"
            alt="tesoura"
            src={Tesoura}
          />
        </div>
      )}

      {playerMove === 1 && (
        <div>
          {" "}
          <img className="icons" alt="papel" src={Papel} />{" "}
        </div>
      )}

      {playerMove === 2 && (
        <div>
          {" "}
          <img className="icons" alt="papel" src={Pedra} />{" "}
        </div>
      )}

      {playerMove === 3 && (
        <div>
          {" "}
          <img className="icons" alt="papel" src={Tesoura} />{" "}
        </div>
      )}

      <div className="playersContainer">
        <div className="playerCard">
          <div>
            <h2 className="playersName">YOU</h2>
            <h2> Score: {scorePlayer}</h2>
          </div>
          <img
            className="players"
            src="https://mlpnk72yciwc.i.optimole.com/cqhiHLc.WqA8~2eefa/w:auto/h:auto/q:75/https://bleedingcool.com/wp-content/uploads/2020/05/rickmortypuppet-1.jpg"
            alt="IA"
          />
        </div>
      </div>
    </>
  );
};

export default PlayerSide;
