import "./App.css";
import GameEngine from "./components/GameEngine";

function App() {
  return (
    <>
      <div className="App">
        <header>
          <h2>JOKENPÔ</h2>
        </header>
        <GameEngine />
        <div className="clear"></div>
      </div>
      <footer>
        <p>
          Todos os direitos <s>não estão</s> reservados
        </p>
      </footer>
    </>
  );
}

export default App;
